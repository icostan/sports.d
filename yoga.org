* Yoga
** 2020
*** Jan
**** 29
     - first class
*** Apr
**** 17
     - padahastasana: first warm sensation at the bottom of the spine
*** May
**** 04
     - paschimottanasana: warm sensation on entire face when breath in
*** Jun
**** 17
     - tratakam: black square with flowing waves
*** Jul
**** 22
     - padahastasana: warm the whole back spine
**** 25
     - tratakam: very clear mental view of the star in the middle
*** Aug
**** 22
     - shambhavi mudra: visualizing the hand with little black ball (not white)
*** Dec
**** 12
     - laya: pratyahara - flying around stars
** 2021
*** Mar
**** 18
     - brahmacharyasana - strong activation of ajna chackra, felt like a warm stamp/tinglings on my forehead
*** Apr
**** 21
     - bhujangasana - empty space in my space while seeing a big red, fire ball
*** Sep
**** 15
     - respiratia yogina completa - vibratii puternice in piept, activare anahata
